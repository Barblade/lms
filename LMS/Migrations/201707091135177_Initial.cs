namespace LMS.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Licenses",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        ExpiryTime = c.DateTime(nullable: false),
                        CustomerName = c.String(nullable: false, maxLength: 40),
                        CustomerEmail = c.String(nullable: false, maxLength: 254),
                        LicenseKey = c.String(nullable: false, maxLength: 10),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Licenses");
        }
    }
}
