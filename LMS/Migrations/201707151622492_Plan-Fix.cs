namespace LMS.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PlanFix : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Plans", "Days", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Plans", "Days");
        }
    }
}
