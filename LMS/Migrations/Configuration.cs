namespace LMS.Migrations
{
    using Models;
    using System;
    using System.Data.Entity.Migrations;

    internal sealed class Configuration : DbMigrationsConfiguration<LMSContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(LMSContext context)
        {
            context.Licenses.AddOrUpdate(i => i.LicenseKey,
                 new License
                 {
                     ExpiryTime = DateTime.Parse("2017/08/05 15:23:11"),
                     CustomerName = "Zbigniew Stonoga",
                     CustomerEmail = "stonoga@gmail.com",
                     Price = 29.99M,
                     LicenseKey = "KIO34MN5HT"
                 },

                 new License
                 {
                     ExpiryTime = DateTime.Parse("2017-06-12 05:45:11"),
                     CustomerName = "Andrzej Lepper",
                     CustomerEmail = "j.kowalski@gmail.com",
                     Price = 29.99M,
                     LicenseKey = "I5KFK1LK64"
                 },
                 
                 new License
                 {
                     ExpiryTime = DateTime.Parse("2017-07-15 12:33:34"),
                     CustomerName = "Adam Nowak",
                     CustomerEmail = "amalysz@gmail.com",
                     Price = 299.99M,
                     LicenseKey = "VK86Q6ZX34"
                 },
                 
                 new License
                 {
                     ExpiryTime = DateTime.Parse("2017-08-01 09:58:05"),
                     CustomerName = "Andrzej Duda",
                     CustomerEmail = "aduda@gmail.com",
                     Price = 79.99M,
                     LicenseKey = "9M5LOQ287Y"
                 },

                 new License
                 {
                     ExpiryTime = DateTime.Parse("2017-10-05 23:44:08"),
                     CustomerName = "Janusz Korwine Mike",
                     CustomerEmail = "jkmikke@gmail.com",
                     Price = 79.99M,
                     LicenseKey = "95MS3Y6MRA"
                 },

                 new License
                 {
                     ExpiryTime = DateTime.Parse("2017-10-05 23:44:08"),
                     CustomerName = "Beata Szydlo",
                     CustomerEmail = "bszydlo@gmail.com",
                     Price = 79.99M,
                     LicenseKey = "95MS3Y6MRX"
                 },

                 new License
                 {
                     ExpiryTime = DateTime.Parse("2017-10-05 23:44:08"),
                     CustomerName = "Jaroslaw Kaczynski",
                     CustomerEmail = "jkaczynskigmail.com",
                     Price = 79.99M,
                     LicenseKey = "95MS3Y6MRA"
                 },

                 new License
                 {
                     ExpiryTime = DateTime.Parse("2017-10-05 23:44:08"),
                     CustomerName = "Joachim Brudzinski",
                     CustomerEmail = "joachimek@gmail.com",
                     Price = 29.99M,
                     LicenseKey = "95MS3Y6MRB"
                 },

                 new License
                 {
                     ExpiryTime = DateTime.Parse("2017-10-05 23:44:08"),
                     CustomerName = "Adam Lipinski",
                     CustomerEmail = "alipinski@gmail.com",
                     Price = 29.99M,
                     LicenseKey = "95MS3Y6MRC"
                 },

                 new License
                 {
                     ExpiryTime = DateTime.Parse("2017-10-05 23:44:08"),
                     CustomerName = "Mariusz Kaminski",
                     CustomerEmail = "mkaminski@gmail.com",
                     Price = 29.99M,
                     LicenseKey = "95MS3Y6MRD"
                 },

                 new License
                 {
                     ExpiryTime = DateTime.Parse("2017-10-05 23:44:08"),
                     CustomerName = "Ryszard Terlecki",
                     CustomerEmail = "rysiu.terlecki@gmail.com",
                     Price = 79.99M,
                     LicenseKey = "95MS3Y6MRE"
                 },

                 new License
                 {
                     ExpiryTime = DateTime.Parse("2017-10-05 23:44:08"),
                     CustomerName = "Mareuk Suski",
                     CustomerEmail = "suskimarek@gmail.com",
                     Price = 29.99M,
                     LicenseKey = "95MS3Y6MRF"
                 },

                 new License
                 {
                     ExpiryTime = DateTime.Parse("2017-10-05 23:44:08"),
                     CustomerName = "Jaroslaw Zielinski",
                     CustomerEmail = "jzielinski@gmail.com",
                     Price = 29.99M,
                     LicenseKey = "95MS3Y6MRG"
                 },

                 new License
                 {
                     ExpiryTime = DateTime.Parse("2017-10-05 23:44:08"),
                     CustomerName = "Kazimierz Marcninkiewicz",
                     CustomerEmail = "kazimierz.marc@gmail.com",
                     Price = 79.99M,
                     LicenseKey = "95MS3Y6MRH"
                 },

                 new License
                 {
                     ExpiryTime = DateTime.Parse("2017-10-05 23:44:08"),
                     CustomerName = "Mariusz Blaszczak",
                     CustomerEmail = "blaszczak@gmail.com",
                     Price = 29.99M,
                     LicenseKey = "95MS3Y6MRI"
                 },

                 new License
                 {
                     ExpiryTime = DateTime.Parse("2017-10-05 23:44:08"),
                     CustomerName = "Tadeusz Cymanski",
                     CustomerEmail = "tcymanski@gmail.com",
                     Price = 29.99M,
                     LicenseKey = "95MS3Y6MRJ"
                 },

                 new License
                 {
                     ExpiryTime = DateTime.Parse("2017-10-05 23:44:08"),
                     CustomerName = "Jolanta Szczypinska",
                     CustomerEmail = "szczypta@gmail.com",
                     Price = 79.99M,
                     LicenseKey = "95MS3Y6MRK"
                 },

                 new License
                 {
                     ExpiryTime = DateTime.Parse("2017-10-05 23:44:08"),
                     CustomerName = "Beata Kempa",
                     CustomerEmail = "kempa.trawy@gmail.com",
                     Price = 79.99M,
                     LicenseKey = "95MS3Y6MRL"
                 },

                 new License
                 {
                     ExpiryTime = DateTime.Parse("2017-10-05 23:44:08"),
                     CustomerName = "Janusz Palikot",
                     CustomerEmail = "palikut@gmail.com",
                     Price = 79.99M,
                     LicenseKey = "95MS3Y6MRM"
                 },

                 new License
                 {
                     ExpiryTime = DateTime.Parse("2017-10-05 23:44:08"),
                     CustomerName = "Stefan Niesiolowski",
                     CustomerEmail = "stefan.niesiolowski@gmail.com",
                     Price = 79.99M,
                     LicenseKey = "95MS3Y6MRN"
                 },

                 new License
                 {
                     ExpiryTime = DateTime.Parse("2017-10-05 23:44:08"),
                     CustomerName = "Adam Szejnfeld",
                     CustomerEmail = "adam.szejnfelld@gmail.com",
                     Price = 29.99M,
                     LicenseKey = "95MS3Y6MRO"
                 },

                 new License
                 {
                     ExpiryTime = DateTime.Parse("2017-10-05 23:44:08"),
                     CustomerName = "Ewa Kopacz",
                     CustomerEmail = "kopacz@gmail.com",
                     Price = 29.99M,
                     LicenseKey = "95MS3Y6MRP"
                 },

                 new License
                 {
                     ExpiryTime = DateTime.Parse("2017-10-05 23:44:08"),
                     CustomerName = "Grzegorz Schetyna",
                     CustomerEmail = "grzesiu.schetyna@gmail.com",
                     Price = 79.99M,
                     LicenseKey = "95MS3Y6MRR"
                 },

                 new License
                 {
                     ExpiryTime = DateTime.Parse("2017-10-05 23:44:08"),
                     CustomerName = "Borys Budka",
                     CustomerEmail = "budka.borys@gmail.com",
                     Price = 79.99M,
                     LicenseKey = "95MS3Y6MRS"
                 }
            );
        }
    }
}
