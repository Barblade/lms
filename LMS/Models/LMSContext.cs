﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace LMS.Models
{
    public class LMSContext : IdentityDbContext<ApplicationUser>
    {
        public LMSContext()
            : base("LMSDBContext")
        {
        }

        public DbSet<License> Licenses { get; set; }
        public DbSet<Plan> Plans { get; set; }
    }
}