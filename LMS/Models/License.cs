﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;

namespace LMS.Models
{
    public class License
    {
        public int ID { get; set; }

        [Display(Name = "Create Date")]
        [Required]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime CreateTime
        {
            get
            {
                return DateTime.Now;
            }
        }

        [Display(Name = "Expiry Time")]
        [Required]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd HH:mm:ss}", ApplyFormatInEditMode = true)]
        public DateTime ExpiryTime { get; set; }

        [Display(Name = "Days Left")]
        public uint DaysLeft
        {
            get
            {
                return (uint)(ExpiryTime - CreateTime).TotalDays;
            }
        }

        [Display(Name = "Customer Name")]
        [Required]
        [StringLength(40)]
        public string CustomerName { get; set; }

        [Display(Name = "Customer E-Mail")]
        [Required]
        [DataType(DataType.EmailAddress)]
        [StringLength(254)]
        public string CustomerEmail { get; set; }

        [Display(Name = "License Key")]
        [Required]
        [StringLength(10, MinimumLength = 10)]
        public string LicenseKey { get; set; }

        [DataType(DataType.Currency), Range(1, 1000)]
        public decimal Price { get; set; }
    }
}