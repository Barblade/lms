﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace LMS.Models
{
    public class Plan
    {
        public int ID { get; set; }

        [Required]
        [StringLength(40)]
        public string Name { get; set; }

        [Required]
        [Range(0, Int32.MaxValue)]
        public int Days { get; set; }

        [Required]
        [DataType(DataType.Currency), Range(1, 1000)]
        public decimal Price { get; set; }
    }
}
